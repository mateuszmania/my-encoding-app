package wwsis.lista4;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class MyFrame {

	public static void open() {
		final JFrame okno = new JFrame("Szyfruj/Deszyfruj");
		okno.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		okno.setSize(600, 200);

		JLabel lZrodlo = new JLabel();
		lZrodlo.setText("Plik zrodlowy:");
		JTextField zrodlo = new JTextField();
		JButton bZrodlo = new JButton("Wybierz");

		JLabel lKlucz = new JLabel();
		lKlucz.setText("Klucz:");
		JTextField klucz = new JTextField();
		JButton bKlucz = new JButton("Wybierz");

		JLabel lDocelowy = new JLabel();
		lDocelowy.setText("Plik docelowy:");
		JTextField docelowy = new JTextField();
		JButton bDocelowy = new JButton("Wybierz");

		JButton szyfruj = new JButton("Szyfruj/Deszyfruj");

		JPanel column1 = new JPanel();
		column1.setLayout(new GridLayout(3, 1, 0, 15));
		column1.add(lZrodlo);
		column1.add(lKlucz);
		column1.add(lDocelowy);

		JPanel column2 = new JPanel();
		column2.setLayout(new GridLayout(3, 1, 0, 15));
		column2.add(zrodlo);
		zrodlo.setColumns(20);
		column2.add(klucz);
		column2.add(docelowy);

		JPanel column3 = new JPanel();
		column3.setLayout(new GridLayout(3, 1, 0, 15));
		column3.add(bZrodlo);
		column3.add(bKlucz);
		column3.add(bDocelowy);

		JPanel line1 = new JPanel();
		line1.setLayout(new BoxLayout(line1, BoxLayout.PAGE_AXIS));
		line1.setBorder(BorderFactory.createEmptyBorder(15, 5, 5, 0));
		line1.add(szyfruj);
		szyfruj.setAlignmentX(Component.RIGHT_ALIGNMENT);
		
		setButton(bZrodlo, zrodlo);
		setButton(bKlucz, klucz);
		setButton(bDocelowy, docelowy);

		letEncode(zrodlo, klucz, docelowy, szyfruj);

		Container contentPane = okno.getContentPane();
		contentPane.setLayout(new BorderLayout());
		contentPane.add(column1, BorderLayout.LINE_START);
		contentPane.add(column2, BorderLayout.CENTER);
		contentPane.add(column3, BorderLayout.LINE_END);
		contentPane.add(line1, BorderLayout.PAGE_END);
		okno.setVisible(true);
		

	}

	private static void letEncode(JTextField zrodlo, JTextField klucz, JTextField docelowy, JButton szyfruj) {
		szyfruj.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if ((zrodlo.getText().isEmpty()) || (docelowy.getText().isEmpty())) {
					Component frame = new JFrame();
					JOptionPane.showMessageDialog(frame, "Wprowadz poprawne dane!", "B��d!",
							JOptionPane.WARNING_MESSAGE);
				} else {
					try {
						Enigma.encryptDecrypt(zrodlo, klucz, docelowy);
					} catch (IOException e1) {
						e1.printStackTrace();
					}
				}
			}
		});
	}
	
	private static void setButton (JButton jButt, JTextField tField) {	
		jButt.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JFileChooser c = new JFileChooser();
				int rVal = c.showOpenDialog(c);
				if (rVal == JFileChooser.APPROVE_OPTION) {
					tField.setText(c.getSelectedFile().toString());
				}
				if (rVal == JFileChooser.CANCEL_OPTION) {
					tField.setText("You pressed cancel");
				}
			}
		});		
	}
}
