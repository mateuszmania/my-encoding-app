package wwsis.lista4;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import javax.swing.JTextField;

public class Enigma {

	public static void encryptDecrypt(JTextField zrodlo, JTextField klucz, JTextField docelowy)
			throws FileNotFoundException, IOException {

		String path1 = zrodlo.getText();
		File file1 = new File(path1);
		FileInputStream zis = new FileInputStream(file1);
		byte[] data1 = new byte[(int) file1.length()];
		zis.read(data1);
		zis.close();

		String path2 = klucz.getText();
		File file2 = new File(path2);
		FileInputStream kis = new FileInputStream(file2);
		byte[] data2 = new byte[(int) file2.length()];
		kis.read(data2);
		kis.close();

		byte[] output = new byte[(int) file1.length()];
		for (int i = 0; i < file1.length(); i++) {
			output[i] = (byte) (data1[i] ^ data2[i % data2.length]);
		}

		String path3 = docelowy.getText();
		System.out.println(path3);
		File file3 = new File(path3);
		FileOutputStream fos = new FileOutputStream(file3);
		fos.write(output, 0, output.length);
		fos.flush();
		fos.close();
	}
}
